FROM python:3.5

RUN groupadd -r uwsgi && useradd -r -g uwsgi uwsgi
RUN pip install flask uWSGI requests redis
WORKDIR /app
ENV FLASK_APP=identidock.py 
ENV API_DNMONSTER="http://dnmonster:8080/monster/"
COPY app /app
RUN chmod a+x *.py
COPY cmd.sh /

EXPOSE 5000 9090 9191
USER uwsgi

CMD ["/cmd.sh"]

